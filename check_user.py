#!/usr/bin/env python3

import yaml
import os
import datetime

conf_file = "user.yml"

if __name__ == "__main__":

    with open(conf_file, 'r') as stream:
        cfg = yaml.safe_load(stream)

    try:
        print( "'first_name': First name is a string:", str(cfg['first_name'] ) )
        print( "'date': Date is a string:", str(cfg['date'] ) )
        try:
                date = datetime.datetime.strptime(str(cfg['date']), "%Y-%m-%d")
                print( "Parsed date:", date)
        except:
                raise ValueError("Cannot parse date. '%s' not ISO8601"%cfg['date'])


    except Exception as ex:
        print( "Config file error")
        print( "Exception was: ", ex )
        exit(1)

    exit(0)

